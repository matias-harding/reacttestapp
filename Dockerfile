# base image
FROM node:12.2.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN yarn install
RUN yarn global add react-scripts@3.0.1
RUN yarn add react-google-maps
RUN yarn add dark-sky-api
RUN yarn add react-bootstrap bootstrap
#RUN yarn add google-map-react


# start app
CMD ["yarn", "run", "start"]
