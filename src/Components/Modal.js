import React, {Component} from 'react';
import {Modal, Button} from 'react-bootstrap';

class DataModal extends Component {

    render(){
        return(
            <Modal
            {...this.props}
            size="sm"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
              {this.props.country}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p><b>Temperatura Actual</b></p>
              <p>
                {this.props.temp}º Celcius
              </p>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
          </Modal>
      
        );
    }
}

export default DataModal