import React from "react";
import DarkSkyApi from 'dark-sky-api';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withGoogleMap, withScriptjs, GoogleMap } from "react-google-maps";

import DataModal from "./Components/DataModal.js";

DarkSkyApi.apiKey = '96513c4f3bffaa9280fad2ff9b876755';
DarkSkyApi.units = 'si';
DarkSkyApi.language = 'es';

function getPosition(event) {

  let position = {
    latitude: event.latLng.lat(), 
    longitude: event.latLng.lng()
  };

  DarkSkyApi.loadCurrent(position)
    .then(result => {
      console.log(result);
    });
  
}

function Map() {

  return (
    <GoogleMap
      defaultZoom={3}
      defaultCenter={{ lat: 17.6781847, lng: 12.7322777 }}
      defaultOptions={{
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        scrollwheel: false
      }}
      onClick={(e) => getPosition(e)}
    >
    </GoogleMap>
  );
}

const MapWrapped = withScriptjs(withGoogleMap(Map));

export default function App() {
  return (
    <div style={{ width: "100vw", height: "100vh" }}>
      <MapWrapped
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${
          "AIzaSyDkZVaZbsSF6vHRfHTPuPTR-fKzomzVPmY"
        }`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `95%` }} />}
      />
      <DataModal show={false}>

      </DataModal>
    </div>
  );
}
