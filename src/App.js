import React, {Component}  from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { withGoogleMap, withScriptjs, GoogleMap } from "react-google-maps";

import DarkSkyApi from './Components/DarkSkyApi.js';
import Modal from "./Components/Modal.js";

class App extends Component {
    constructor(){
        super()
        this.state={
            showModal: false,
            temp: 0,
            country: ""
        }
    }

    handleModal = () => {
        this.setState({showModal:!this.state.showModal})
    }

    componentDidMount = (position) => {
      if(!position){ return }
      console.log(position)
      fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&result_type=country&language=es&key=AIzaSyDkZVaZbsSF6vHRfHTPuPTR-fKzomzVPmY`)
      .then(res => res.json())
      .then((data) => {
        if(data.results[0].formatted_address){
          this.setState({country: data.results[0].formatted_address})
        }
        console.log(data)
      })
      .catch(console.log)
    }

    Map = () => {
        
        return (
          <GoogleMap
            defaultZoom={3}
            defaultCenter={{ lat: 17.6781847, lng: 12.7322777 }}
            defaultOptions={{
              disableDefaultUI: true,
              disableDoubleClickZoom: true,
              scrollwheel: false
            }}
            onClick={(e) => this.getPosition(e)}
          >
          </GoogleMap>
        );
    }

    MapWrapped = withScriptjs(withGoogleMap(this.Map));

    getPosition = (event) => {

        let position = {
            latitude: event.latLng.lat(), 
            longitude: event.latLng.lng()
        };
        
        DarkSkyApi.loadCurrent(position)
        .then(result => {
            console.log(result);
            this.setState({temp: result.temperature})
            this.componentDidMount(position)
            this.handleModal()
        });
    
    }
        
    render() {
        return(
            <div style={{ width: "100vw", height: "100vh" }}>
            <this.MapWrapped
              googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=${
                "AIzaSyDkZVaZbsSF6vHRfHTPuPTR-fKzomzVPmY"
              }`}
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `100%` }} />}
              mapElement={<div style={{ height: `100%` }} />}
            />
            <Modal 
                show={this.state.showModal} 
                onHide={() => this.setState({showModal:false})}
                temp={this.state.temp}
                country={this.state.country}
            />
          </div>
        )
    }
}

export default App;